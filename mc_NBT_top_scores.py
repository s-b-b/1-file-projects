"""Extracts top player scores from files written in NBT format."""
import argparse
from collections import namedtuple
from nbt import nbt

PLAYER_SCORE = namedtuple('PLAYER_SCORE', ['name', 'score'])


def ticks_to_time(ticks):
    sec = ticks // 20
    minutes = (sec // 60) % 60
    hours = sec // 3600
    return '{} hours {} minutes'.format(hours, minutes)


def parse(to_file, from_file, number):
    nbtfile = nbt.NBTFile(from_file, 'rb')
    objective_display_names = {str(tag['Name']): str(tag['DisplayName']) for tag in nbtfile['data']['Objectives'].tags}
    objectives = {str(tag['Name']): {} for tag in nbtfile['data']['Objectives'].tags}
    player_scores = nbtfile['data']['PlayerScores']
    for score in player_scores:
        player_name = str(score['Name'])
        objective = str(score['Objective'])
        objectives[objective][player_name] = int(str(score['Score']))
    with open(to_file, 'w') as f:
        for key, obj in objectives.items():
            if key == 'Player_Deaths':
                l = sorted([PLAYER_SCORE(name, score) for name, score in obj.items()], key=lambda x: x.score)
            elif 'Time' in key:
                l = sorted([PLAYER_SCORE(name, score) for name, score in obj.items()],
                           key=lambda x: x.score,
                           reverse=True)
                l = [PLAYER_SCORE(name, ticks_to_time(score)) for name, score in l]
            else:
                l = sorted([PLAYER_SCORE(name, score) for name, score in obj.items()],
                           key=lambda x: x.score,
                           reverse=True)
            f.write('{}\n'.format(objective_display_names[key]))

            for name, score in l if number <= 0 else l[:number]:
                f.write('{}: {}\n'.format(name, score))
            f.write('\n')


def parser():
    """Take care of parsing arguments from CLI."""
    arg_parser = argparse.ArgumentParser(description='Extracts top player scores from files written in NBT format.')
    arg_parser.add_argument('--n', type=int, default=0,
                            help='Number of scores to save')
    arg_parser.add_argument('--f', type=str, default='scoreboard.dat',
                            help='Name of the file to read scores from')
    arg_parser.add_argument('--t', type=str, default='top_scores.txt',
                            help='Name of the file to save scores to')
    return arg_parser.parse_args()


if __name__ == '__main__':
    args = parser()
    parse(args.t, args.f, args.n)
