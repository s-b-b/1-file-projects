"""Randomly selects movie from movies in directory."""

import random
import os

movieExtensions = ['.webm', '.mkv', '.flv', '.flv', '.vob', '.ogv',
                   '.ogg', '.drc', '.gif', '.gifv', '.mng', '.avi',
                   '.mov', '.qt', '.wmv', '.yuv', '.rm', '.rmvb',
                   '.asf', '.amv', '.mp4', '.m4p', '.m4v', '.mpg',
                   '.mp2', '.mpeg', '.mpe', '.mpv', '.mpg', '.mpeg',
                   '.m2v', '.m4v', '.svi', '.3gp', '.3g2', '.mxf',
                   '.roq', '.nsv', '.flv', '.f4v', '.f4p', '.f4a', '.f4b']


def main():
    files = os.listdir(os.getcwd())
    movies = [filename for filename in files
              if os.path.splitext(filename)[1] in movieExtensions]
    print(random.choice(movies))


if __name__ == '__main__':
    main()
