from PIL import Image
import os


folder = os.path.join(os.getcwd(), 'photos')
images = [Image.open(entry.path) for entry in os.scandir(folder) if entry.is_file()]
output_image = Image.new('RGB', (images[0].width, images[0].height*len(images)))
frame_height = 400
for row in range(0, images[0].height, frame_height):
	for i, image in enumerate(images):
		frame = image.crop((0, row, image.width, row + frame_height))
		output_image.paste(frame, (0, row*len(images) + i*frame_height))
output_image.save(os.path.join(os.getcwd(), 'scrambled_photo.jpg'))		
